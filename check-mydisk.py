#!/usr/bin/env python
"""
check-mydisk.py adalah script python yang digunakan untuk melakukan pemeriksaan
penggunaan kapasitas disk (penyimpanan). Hasil pemeriksaan disimpan  ke dalam CSV File

PYTHON 2.7

DEPENDENCY:
   - cvs

LICENSE:
   Copyright 2021 
   Original Author: Henry Saptono <boypyt@gmail.com>
   Released under GPL. see LICENSE for details.

"""

import argparse
import sys
import subprocess
import time
import csv

def print_help(parser):
    parser.print_help()
    exit(2)

def check_disk(_args):
    if _args.filesys == None:
        cmd = "/bin/df -h -x tmpfs /"
    else:
        cmd = "/bin/df -h -x tmpfs %s"%_args.filesys
    df_output = None

    try:
        df_output = subprocess.check_output(cmd.split())
        _date = time.strftime('%Y-%m-%d')
        _time = time.strftime('%H:%M:%S')
        _entry =  df_output.decode().split('\n')
        header = _entry[0].split()[:-1]
        header[-1] = "Mounted On"
    except:
        print("failed to execute df ")
        exit(2)
    _entry.pop(0)
    _entry.pop(-1)
    _entries = []
    for _ent in _entry: 
       _entri_dict ={}
       _entri_dict['Date'] = _date
       _entri_dict['Time'] = _time
       x = 0
       for rec in _ent.split():
           _entri_dict[header[x]] = rec.strip('%')
           x = x + 1
       _entries.append(_entri_dict)
    #print(_time)
    header.insert(0,'Datetime')
    #print(header)
    #print(_entry)
    return _entries

def save_csv(_entries,fname="report_check_disk.csv"):
    keys = ['Date','Time', 'Filesystem', 'Size', 'Used', 'Avail', 'Use%', 'Mounted On']
    try:
        f = open(fname)
        f.close()

        with open(fname, 'a')  as output_file:
             dict_writer = csv.DictWriter(output_file, keys)
             dict_writer.writerows(_entries)

    except IOError:
        with open(fname, 'a')  as output_file:
             dict_writer = csv.DictWriter(output_file, keys)
             dict_writer.writeheader()
             dict_writer.writerows(_entries)
        

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Check Disk Filesystem Usage and Save to CSV")
    parser.add_argument('-o', '--output',
                        type=str,
                        metavar='OUTPUT FILENAME',
                        help='output filename (CSV)')
    parser.add_argument('-f', '--filesys',
                        type=str,
                        metavar='ADDITIONAL FILESYSTEMS',
                        help='Filesystems which monitored,\ndefault is only / Root filesystem\nex: -f "/home /var"')
    if (len(sys.argv) < 1):
        print_help(parser)

    _args = parser.parse_args()
    #save_xlsx(check_disk(_args))
    if _args.output :
       save_csv(check_disk(_args),_args.output)
    else:
       save_csv(check_disk(_args))
